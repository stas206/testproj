#include <QCoreApplication>
#include <stdlib.h>
#include <iostream>
#include <QString>
#include <string>
#include <algorithm>

#include <deque>
#include <vector>

using std::cout;
using std::endl;
using std::string;
using std::vector;

class Arhive
{
public:
    typedef vector<char>::iterator iterator;
    typedef vector<char> conteiner;

    conteiner& arhive;

    Arhive(conteiner& arh,size_t = 0): arhive(arh), currPos(0), separator('|')
    {}

    template<typename T>
    Arhive& operator<<( T & value )
    {
        arhive.insert((arhive).end(), (char*)&value, (char*)&value + sizeof(T));
        arhive.push_back(separator);

        return *this;
    }

    Arhive & operator<<( string & value )
    {
        if (!value.empty())
            arhive.insert((arhive).end(), value.begin() , value.end());

        arhive.push_back('\0');
        return *this;
    }


    template < typename T>
    Arhive & operator>>( T & value)
    {
        union Bits
        {   T value;
            char buffer[sizeof(T)];
        };

        Bits bitValue;
        iterator it(arhive.begin()+currPos);
        iterator itfind = std::find(it, arhive.end() , separator);

        for (size_t i = 0; i < sizeof(T);++i)
        {
            bitValue.buffer[i] = '\0';
            if( *it != separator )
            {
                bitValue.buffer[i] = *it;
                ++it;
            }
        }
        value = bitValue.value;
        currPos = std::distance(arhive.begin(),++itfind);

        return *this;
    }

    Arhive & operator>>( string &value )
    {
        iterator itfind = std::find(arhive.begin()+currPos, arhive.end(), '\0');
        value.clear();
        value.insert(value.begin(), arhive.begin()+currPos, itfind);

        currPos = std::distance(arhive.begin(),++itfind);
        return *this;
    }

    void clear()
    {
        arhive.clear();
        currPos = 0;
    }
    void setSeparator(const char sep)
    {
        separator = sep;
    }

private:
    size_t currPos;
    char separator;

};

typedef struct alarm_information
{
//    friend class Arhive;

    alarm_information():
        isAlarm(0),
        timeAlarm(0),
        alarmValue(0.0),
        equipment("asdfwqerxcvxzcv"),
        color("#ffff00")
    {
    }

    int                 ident;
    unsigned char       isAlarm;
    unsigned char       isNeedAck;
    unsigned char       isNeedSave;
    unsigned char       state;
    time_t              timeAlarm;
    time_t              timeAck;
    float               alarmValue;
    float               lowLimitValue;
    float               highLimitValue;
    float               equal;
    float               noEqual;
    float               setPointValue;
    string             equipment;
    string             color;
    char                needSend;
    unsigned char       bell;


    void deserialize(Arhive &arh)
    {

        arh >> ident;

        arh >> isAlarm;
        arh >> isNeedAck;
        arh >> isNeedSave;
        arh >> state;

        arh >> timeAlarm;
        arh >> timeAck;
        arh >> alarmValue;
        arh >> lowLimitValue;
        arh >> highLimitValue;

        arh >> equal;
        arh >> noEqual;
        arh >> setPointValue;

        arh >> needSend;
        arh >> bell;

        arh >> equipment;
        arh >> color;

    }

    void serialize(Arhive &arh)
    {

        arh << ident;                 // int                 ident;

        arh << isAlarm;               // unsigned char       isAlarm;
        arh << isNeedAck;             // unsigned char       isNeedAck;
        arh << isNeedSave;            // unsigned char       isNeedSave;
        arh << state;                 // unsigned char       state;

        arh << timeAlarm;             // time_t              timeAlarm;
        arh << timeAck;               // time_t              timeAck;

        arh << alarmValue;            // float               alarmValue;
        arh << lowLimitValue;         // float               lowLimitValue;
        arh << highLimitValue;        // float               highLimitValue;
        arh << equal;                 // float               equal;
        arh << noEqual;               // float               noEqual;
        arh << setPointValue;         // float               setPointValue;

        arh << needSend;              // char                needSend;
        arh << bell;                  // unsigned char       bell;

        arh << equipment;             // string             equipment;
        arh << color;                 // string             color;

    }
} AlarmInfo;




int main(int argc, char *argv[])
{
    QCoreApplication aplicw(argc, argv);

    AlarmInfo myAInfo;
    myAInfo.ident = 5;


    //typedef vector<char>::iterator iterator;
    vector<char> buf;

    Arhive arh(buf);

    myAInfo.serialize(arh);
    string bodystring("sdfsalkdfjxcv.,mxczv,.xczmv.,mdflksajdhfkljwheyriuyskdljfhkjsdfh");
    arh << bodystring;

    //&buf[0]
    //char buf[1024] = (char*)&myAInfo;

    //char buf[1024] = *myAInfo.serialize();
    //
    Arhive recivebuf(buf);
    myAInfo.deserialize(recivebuf);
    string body;
    recivebuf >> body;


    cout<< "sadasdas = "<<buf.size()<<"      -<"<<&buf[0] <<">"<<endl;



    return aplicw.exec();
}
